<?php

Route::group(['middleware' => ['web', 'admin'], 'prefix' => 'admin'], function () {
    Route::get('/', 'FritsStegmann\LaravelQS\Controller\AdminHomeController@index');
    Route::get('/users', 'FritsStegmann\LaravelQS\Controller\UserController@index');

    Route::get('/users/create', 'FritsStegmann\LaravelQS\Controller\UserController@create');
    Route::put('/users', 'FritsStegmann\LaravelQS\Controller\UserController@store');
    Route::get('/users/{id}', 'FritsStegmann\LaravelQS\Controller\UserController@show');
    Route::post('/users/{id}', 'FritsStegmann\LaravelQS\Controller\UserController@update');
    Route::post('/users/{id}/password', 'FritsStegmann\LaravelQS\Controller\UserController@updatePassword');
    Route::get('/users/{id}/edit', 'FritsStegmann\LaravelQS\Controller\UserController@edit');
    Route::delete('/users/{id}', 'FritsStegmann\LaravelQS\Controller\UserController@delete');

    Route::get('/failed_jobs', 'FritsStegmann\LaravelQS\Controller\FailedJobController@index');
    Route::get('/failed_jobs/{id}', 'FritsStegmann\LaravelQS\Controller\FailedJobController@show');
});