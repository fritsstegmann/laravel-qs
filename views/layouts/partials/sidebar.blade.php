<div class="sidebar-sticky">
    <ul class="nav flex-column">
        <li class="nav-item">
            <a class="nav-link p-3 m-2 {{ isActiveURL('/admin/') }}" href="/admin">
                <span class="fa fa-home"></span>
                Home
            </a>
            <a class="nav-link p-3 m-2 {{ isActiveURL('/admin/users') }}" href="/admin/users">
                <span class="fa fa-users"></span>
                Users
            </a>
            <a class="nav-link p-3 m-2 {{ isActiveURL('/admin/failed_jobs') }}" href="/admin/failed_jobs">
                <span class="fa fa-database"></span>
                Failed Jobs
            </a>
        </li>
    </ul>
</div>