@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin/failed_jobs">Failed Jobs</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $failedJob->failed_at }}</li>
                    </ol>
                </nav>

                <ul class="list-group">
                    <li class="list-group-item">
                        <div class="text-muted">ID</div>
                        <div>{{ $failedJob->id }}</div>
                    </li>
                    <li class="list-group-item">
                        <div class="text-muted">Connection</div>
                        <div>{{ $failedJob->connection }}</div>
                    </li>
                    <li class="list-group-item">
                        <div class="text-muted">Queue</div>
                        <div>{{ $failedJob->queue }}</div>
                    </li>
                </ul>

                <div class="bg-white mt-4 p-4 bg-border">
                    {!! nl2br($failedJob->payload) !!}
                </div>

                <div class="bg-white mt-4 p-4 bg-border">
                    {!! nl2br($failedJob->exception) !!}
                </div>
            </div>
        </div>
    </div>
@endsection
