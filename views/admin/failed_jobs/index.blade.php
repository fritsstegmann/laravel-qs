@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <table class="table bg-white bg-border">
                    <thead>
                    <tr>
                        <th>Failed At</th>
                        <th>Connection</th>
                        <th>Queue</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($failedJobs as $failedJob)
                        <tr>
                            <td><a href="/admin/failed_jobs/{{ $failedJob->id }}">{{ $failedJob->failed_at }}</a></td>
                            <td>{{ $failedJob->connection }}</td>
                            <td>{{ $failedJob->queue }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
