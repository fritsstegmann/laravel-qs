@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin/users">Users</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Create</li>
                    </ol>
                </nav>

                <form action="/admin/users" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="put">

                    <div class="form-group">
                        <label for="name-324234" class="col-form-label">Name</label>
                        <input id="name-324234" class="form-control" type="text" value="" name="name">
                    </div>

                    <div class="form-group">
                        <label for="email-575674" class="col-form-label">Email</label>
                        <input id="email-575674" class="form-control" type="text" value="" name="email">
                    </div>

                    <div class="form-group">
                        <label for="password-238932" class="col-form-label">Password</label>
                        <input id="password-238932" class="form-control" type="password" value="" name="password">
                    </div>

                    <div class="form-group">
                        <label for="role-342534" class="col-form-label">Role</label>
                        <select class="form-control" name="role" id="role-342534">
                            <option selected="selected" value="1">User</option>
                            <option value="2">Admin</option>
                        </select>
                    </div>

                    <div class="pt-4">
                        <a href="/admin/users" class="btn btn-warning">Cancel</a>
                        <input class="btn btn-primary" type="submit" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
