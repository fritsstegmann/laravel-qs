@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin/users">Users</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $user->name }}</li>
                    </ol>
                </nav>

                <form action="/admin/users/{{ $user->id }}" method="post">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="name-324234" class="col-form-label">Name</label>
                        <input id="name-324234" class="form-control" type="text" value="{{ $user->name }}" name="name">
                        @if (isset($errors) && $errors->has('name'))
                            <div class="invalid-feedback">{{ $errors->first('name') }}</div>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="email-575674" class="col-form-label">Email</label>
                        <input id="email-575674" class="form-control" type="text" value="{{ $user->email }}" name="email">
                        @if (isset($errors) && $errors->has('email'))
                            <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="role-342534" class="col-form-label">Role</label>
                        <select class="form-control" name="role" id="role-342534">
                            <option {{ $user->role == 1 ? 'selected': '' }} value="1">User</option>
                            <option {{ $user->role == 2 ? 'selected': '' }} value="2">Admin</option>
                        </select>
                        @if (isset($errors) && $errors->has('role'))
                            <div class="invalid-feedback">{{ $errors->first('role') }}</div>
                        @endif
                    </div>

                    <div class="pt-2 pb-4">
                        <a href="/admin/users/{{ $user->id }}" class="btn btn-warning">Cancel</a>
                        <input class="btn btn-primary" type="submit" value="Save">
                    </div>
                </form>
                <form action="/admin/users/{{ $user->id }}/password" method="post">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="password-231904" class="col-form-label">Password</label>
                        <input id="password-231904" class="form-control" type="text" value="" name="password">
                        @if ($errors->has('password'))
                            <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                        @endif
                    </div>

                    <div class="pt-2">
                        <input class="btn btn-primary" type="submit" value="Update Password">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
