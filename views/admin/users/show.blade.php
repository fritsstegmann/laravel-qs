@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/admin/users">Users</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $user->name }}</li>
                    </ol>
                </nav>

                <ul class="list-group">
                    <li class="list-group-item">
                        <div class="text-muted">ID</div>
                        <div>{{ $user->id }}</div>
                    </li>
                    <li class="list-group-item">
                        <div class="text-muted">Name</div>
                        <div>{{ $user->name }}</div>
                    </li>
                    <li class="list-group-item">
                        <div class="text-muted">Email</div>
                        <div>{{ $user->email }}</div>
                    </li>
                    <li class="list-group-item">
                        <div class="text-muted">Role</div>
                        <div>{{ $user->roleLabel() }}</div>
                    </li>
                </ul>

                <div class="pt-4">
                    <a href="/admin/users/{{ $user->id }}/edit" class="btn btn-primary">Edit</a>
                    <form onsubmit="return confirm('Do you really want to delete {{ $user->name }}?');" class="form-inline d-inline" action="/admin/users/{{ $user->id }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="delete">

                        <input class="btn btn-danger" type="submit" value="Delete">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
