<?php

namespace FritsStegmann\LaravelQS\Controller;

use App\User;
use FritsStegmann\LaravelQS\Request\UserCreateRequest;
use FritsStegmann\LaravelQS\Request\UserUpdatePasswordRequest;
use FritsStegmann\LaravelQS\Request\UserUpdateRequest;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        return response()->view('admin.users.index', ['users' => User::paginate(15)]);
    }

    public function show($id)
    {
        $user = User::find($id);

        return response()->view('admin.users.show', ['user' => $user]);
    }

    public function create()
    {
        return response()->view('admin.users.create');
    }

    public function store(UserCreateRequest $request)
    {
        $attributes = $request->all();
        $attributes['password'] = Hash::make($attributes['password']);
        $user = User::create($attributes);

        $user->role = $attributes['role'];
        $user->save();

        return response()->redirectTo('/admin/users/' . $user->id);
    }

    public function edit($id)
    {
        $user = User::find($id);

        return response()->view('admin.users.edit', ['user' => $user]);
    }

    public function updatePassword(UserUpdatePasswordRequest $request, $id)
    {
        $attributes = $request->all();
        $attributes['password'] = Hash::make($attributes['password']);

        $user = User::find($id);
        $user->update($attributes);

        return response()->redirectTo('/admin/users/' . $id);
    }

    public function update(UserUpdateRequest $request, $id)
    {
        $attributes = $request->all();

        $user = User::find($id);
        $user->update($attributes);

        return response()->redirectTo('/admin/users/' . $id);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete($id)
    {
        /** @var User $user */
        $user = User::find($id);
        $user->delete();

        return response()->redirectTo('/admin/users');
    }
}
