<?php

namespace FritsStegmann\LaravelQS\Controller;

class AdminHomeController extends Controller
{
    public function index()
    {
        return response()->view('admin.index.index');
    }
}
