<?php

namespace FritsStegmann\LaravelQS\Controller;

use FritsStegmann\LaravelQS\Models\FailedJob\FailedJob;

class FailedJobController extends Controller
{
    public function index()
    {
        return response()->view('admin.failed_jobs.index', ['failedJobs' => FailedJob::paginate(15)]);
    }

    public function show($id)
    {
        $failedJob = FailedJob::find($id);

        return response()->view('admin.failed_jobs.show', ['failedJob' => $failedJob]);
    }
}
