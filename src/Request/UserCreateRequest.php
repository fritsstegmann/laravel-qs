<?php

namespace FritsStegmann\LaravelQS\Request;

use Illuminate\Foundation\Http\FormRequest;

class UserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|string',
            'role' => 'required|numeric',
            'email' => 'required|email|unique:users',
            'name' => 'required'
        ];
    }
}
