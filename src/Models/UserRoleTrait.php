<?php
/**
 * Created by IntelliJ IDEA.
 * User: frits
 * Date: 2018/08/15
 * Time: 20:11
 */

namespace FritsStegmann\LaravelQS\Models\FailedJob;


trait UserRoleTrait
{
    public $roles = ['user', 'admin'];

    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
        $this->fillable[] = 'role';
    }

    public function isAdmin()
    {
        return $this->role == 2;
    }

    public function roleLabel()
    {
        return $this->role == 1 ? 'User' : 'Admin';
    }
}