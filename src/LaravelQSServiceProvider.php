<?php

namespace FritsStegmann\LaravelQS;

use FritsStegmann\LaravelQS\Auth\JWTGeneratorService;
use FritsStegmann\LaravelQS\Auth\JwtGuard;
use FritsStegmann\LaravelQS\Auth\AdminGuard;
use FritsStegmann\LaravelQS\Commands\GenerateJWTCommand;
use FritsStegmann\LaravelQS\Commands\UserCreateCommand;
use FritsStegmann\LaravelQS\Commands\UserListCommand;
use Illuminate\Contracts\Http\Kernel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class LaravelQSServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @param Kernel $kernel
     */
    public function boot(Kernel $kernel)
    {
        $this->publishes([
            __DIR__ . '/../config/jwt.php' => config_path('jwt.php'),
        ]);

        $this->publishes([
            __DIR__ . '/../assets/dist' => public_path('vendor/qs'),
        ], 'public');

        $this->publishes([
            __DIR__ . '/../views' => resource_path('views'),
        ]);

        $this->loadRoutesFrom(__DIR__ . '/../routes.php');

        $this->loadMigrationsFrom(__DIR__.'/../migrations');

        Auth::extend('admin', function ($app, $name, array $config) {
            // Return an instance of Illuminate\Contracts\Auth\Guard...

            return new AdminGuard(
                Auth::createUserProvider($config['provider']),
                $app['request']
            );
        });

        Auth::extend('jwt', function ($app, $name, array $config) {
            // Return an instance of Illuminate\Contracts\Auth\Guard...

            return new JwtGuard(
                Auth::createUserProvider($config['provider']),
                $app['request']
            );
        });

        $this->app['router']->aliasMiddleware('admin', \FritsStegmann\LaravelQS\Middleware\AdminSecurityMiddleware::class);

        if ($this->app->runningInConsole()) {
            $this->commands([
                UserCreateCommand::class,
                UserListCommand::class,
                GenerateJWTCommand::class
            ]);
        }
    }

    /**
     * Register any application services.
     */
    public function register()
    {
        $this->app->singleton(JWTGeneratorService::class, function () {
            return new JWTGeneratorService();
        });
    }
}
