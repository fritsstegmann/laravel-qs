<?php

namespace FritsStegmann\LaravelQS\Middleware;

use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class AdminSecurityMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::user() && Auth::user()->role == 2) {
            return $next($request);
        } else {
            return redirect('/home');
        }
    }
}
